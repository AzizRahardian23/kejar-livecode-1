
Rails.application.routes.draw do
 
  root 'students#index'
  resources :students
  resources :exams
  resources :teachers
  resources :reports
  resources :pockets
  resources :payments
 
end
