class CreateReports < ActiveRecord::Migration[6.0]
  def change
    create_table :reports do |t|
      t.string :title
      t.string :hasil
      t.string :mapel
      t.string :teacher_id
      t.string :student_id
      t.date :date

      t.timestamps
    end
  end
  def down 
    drop_table :exams
  end
end
