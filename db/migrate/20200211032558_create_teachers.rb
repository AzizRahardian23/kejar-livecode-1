class CreateTeachers < ActiveRecord::Migration[6.0]
  def up
    create_table :teachers do |t|
      t.string :NIK
      t.string :nama
      t.integer :age
      t.string :kelas
      t.string :mapel

      t.timestamps
    end
  end
  def down 
    drop_table :exams
  end
end
