class ReportsController < ApplicationController
    def new
      @report = Report.new
    end    
    def create #untuk memperoses data baru yang dimasukkan di form new
      report = Report.new(resources_params)
      report.save 
      flash[:notice] = 'Report has been created'
      redirect_to reports_path
    end   
    def edit #menampilkan data yang sudah disimpan
      @report = Report.find(params[:id])
    end  
    def update #melakukan proses ketika user mengedit data
      @report = Report.find(params[:id])
      @report.update(resources_params)
      flash[:notice] = 'Report has been update'
      redirect_to report_path(@report)
    end    
    def destroy #untuk menghapus data
      @report = Report.find(params[:id])
      @report.destroy
      flash[:notice] = 'Report has been destroy'
      redirect_to reports_path(@report)
    end  
    def index #menampilkan seluruh data yang ada di database
      @reports = Report.all
    end    
    def show #menampilkan  sebuah data sectemplateara detail
      id = params[:id]
      @report = Report.find(id)
      # render plain: id
      # render plain: @book.title 
     end
    private   
     def resources_params
      params.required(:report).permit(:title, :mapel, :teacher_id, :student_id, :date)
    end  
  end
  
