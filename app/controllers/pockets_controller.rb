class PocketsController < ApplicationController
  def new
    @pocket = Pocket.new
  end    
  def create #untuk memperoses data baru yang dimasukkan di form new
    pocket = Pocket.new(resources_params)
    pocket.save 
    flash[:notice] = 'Pocket has been created'
    redirect_to pockets_path
  end   
  def edit #menampilkan data yang sudah disimpan
    @pocket = Pocket.find(params[:id])
  end  
  def update #melakukan proses ketika user mengedit data
    @pocket = Pocket.find(params[:id])
    @pocket.update(resources_params)
    flash[:notice] = 'Pocket has been update'
    redirect_to pocket_path(@pocket)
  end    
  def destroy #untuk menghapus data
    @pocket = Pocket.find(params[:id])
    @pocket.destroy
    flash[:notice] = 'Pocket has been destroy'
    redirect_to pockets_path(@pocket)
  end  
  def index #menampilkan seluruh data yang ada di database
    @pockets = Pocket.all
  end    
  def show #menampilkan  sebuah data sectemplateara detail
    id = params[:id]
    @pocket = Pocket.find(id)
    # render plain: id
    # render plain: @book.title 
   end
  private   
   def resources_params
    params.required(:pocket).permit(:title, :mapel, :teacher_id, :student_id, :date)
  end  
end
