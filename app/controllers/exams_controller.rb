class ExamsController < ApplicationController
  def new
    @exam = Exam.new
  end    
  def create #untuk memperoses data baru yang dimasukkan di form new
    exam = Exam.new(resources_params)
    exam.save 
    flash[:notice] = 'Exam has been created'
    redirect_to exams_path
  end   
  def edit #menampilkan data yang sudah disimpan
    @exam = Exam.find(params[:id])
  end  
  def update #melakukan proses ketika user mengedit data
    @exam = Exam.find(params[:id])
    @exam.update(resources_params)
    flash[:notice] = 'Exam has been update'
    redirect_to exam_path(@exam)
  end    
  def destroy #untuk menghapus data
    @exam = Exam.find(params[:id])
    @exam.destroy
    flash[:notice] = 'Exam has been destroy'
    redirect_to exams_path(@exam)
  end  
  def index #menampilkan seluruh data yang ada di database
    @exams = Exam.all
  end    
  def show #menampilkan  sebuah data sectemplateara detail
    id = params[:id]
    @exam = Exam.find(id)
    # render plain: id
    # render plain: @book.title 
   end
  private   
   def resources_params
    params.required(:exam).permit(:title, :mapel, :duration, :nilai, :aktifnonaktif, :level, :student_id)
  end  
end
